'use strict';

const fs = require('fs');
const Promise = require('bluebird');
const axios = require('axios');

let filename = `results.json`;
let resultData = fs.readFileSync(filename);
let results = JSON.parse(resultData);

let usermetaPointsUrl = `http://10.11.31.13:8080/api/user_meta/change/reward_points`;
let usermetaKeyUrl = `http://10.11.31.13:8080/api/user_meta/actions/buy_key`;

let doneScript = [];

let timer = ms => new Promise(res => setTimeout(res, ms));

const bulkRequestKey = async (id) => {

    for (var i = 0; i < 999; i++) {
        let bodyKey = {
            user_meta: {
                id: id,
            },
            key: 1,
        };
        let requestKeys = await axios.patch(usermetaKeyUrl, bodyKey);
        console.log(`Done bulk request key: ${i+1} for user id: ${id}`);
        await timer(100);
    }
    console.log(`Done bulk request key!`);
    doneScript.push({
        user_id: id
    });
    return;
};

const execute = () => {
    return Promise.mapSeries(results, async (result) => {
        let body = {
            id: result.user_id,
            delta: 9990,
        };
        // let requestPoints = await axios.patch(usermetaPointsUrl, body);
        await timer(1000);
        console.log(`Done request points for user id: ${result.user_id}`);
        const bulkKey = await bulkRequestKey(result.user_id);
        return;
    }).delay(1000)
    .then(async () => {
        const createFile = await fs.writeFileSync('done.json', JSON.stringify(doneScript), 'utf8');
        console.log(`Done create file for user who has success inject the key`);
    })
    .catch(async (error) => {
        const createFile = await fs.writeFileSync('donecatch.json', JSON.stringify(doneScript), 'utf8');
        console.log(`Done create file for user who has success inject the key`);
        console.log(error);
    });
};

execute();