'use strict';

const fs = require('fs');
const Promise = require('bluebird');
const csv = require('csvtojson');

let filename1 = 'list_phone_number.csv';
let filename2 = 'staging.json';

let resultData = fs.readFileSync(filename2);
let results = JSON.parse(resultData);

let arr1 = [];
let arr2 = [];
let arr3 = [];

// console.log(results.length);

csv()
    .fromFile(filename1)
    .then(async (jsonObj) => {
        const res = await Promise.mapSeries(jsonObj, (obj) => arr1.push(obj.phone_number));
        const res2 = await Promise.mapSeries(results, (obj) => arr2.push(obj.phone_number));


        // await Promise.mapSeries(results, async (data) => {
        //     if (!arr1.includes(data.phone_number)) {
        //         console.log(data.phone_number);
        //         arr2.push(data.phone_number);
        //     }
        // })
        // .then(() => {
        //     console.log(arr2);
        //     console.log(arr2.length);
        // })
        return arr1;
    })
    .then((data) => {
        // return Promise.mapSeries(results, (res) => {
        //     console.log(res.phone_number);
        // });
        // console.log(results);
        console.log(data.length);
        console.log(arr2.length);

        return Promise.mapSeries(data, (d) => {
            if (!arr2.includes(d)) {
                arr3.push({
                    phone_number: d
                })
                console.log(d)
            }
        })
        .then(async () => {
            const createFile = await fs.writeFileSync('list.json', JSON.stringify(arr3), 'utf-8');
        });
    });
