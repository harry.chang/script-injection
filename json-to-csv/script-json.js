'use strict';

const fs = require('fs');
const Promise = require('bluebird');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
    path: 'out-prod.csv',
    header: [
        {id: 'product_id', title: 'product_id'},
        {id: 'product_name', title: 'product_name'},
        {id: 'product_slug', title: 'product_slug'},
        {id: 'product_permalink', title: 'product_permalink'},
        {id: 'product_url_image', title: 'product_url_image'},
        {id: 'price', title: 'price' },
        {id: 'regular_price', title: 'regular_price'},
        {id: 'status', title: 'status'},
        {id: 'stock_status', title: 'stock_status'},
        {id: 'stock_quantity', title: 'stock_quantity'},
        {id: 'total_sales', title: 'total_sales'},
        {id: 'course_id', title: 'course_id'},
        {id: 'course_name', title: 'course_name'},
        {id: 'course_url', title: 'course_url'},
        {id: 'mentor_id', title: 'mentor_id'},
        {id: 'published_at', title:'published_at'},
    ],
})

let fileName = 'prod-products.json';
let resultData = fs.readFileSync(fileName);
let results = JSON.parse(resultData);

let records = [];

Promise.mapSeries(results, (result) => {
    let obj = {};
    obj.product_id = result.product_id;
    obj.product_name = result.product_name;
    obj.product_slug = result.product_slug;
    obj.product_permalink = result.product_permalink;
    obj.product_url_image = result.product_url_image;
    obj.price = result.price;
    obj.regular_price = result.regular_price;
    obj.status = result.status;
    obj.stock_status = result.stock_status;
    obj.stock_quantity = result.stock_quantity;
    obj.total_sales = result.total_sales;
    obj.published_at = result.published_at;

    if (result.courses.length == 1) {
        obj.course_id = result.courses[0].id || "";
        obj.course_name = result.courses[0].name || "";
        obj.course_url = result.courses[0].url || "";
        obj.mentor_id = result.courses[0].mentor_id || "";
    } else {
        obj.course_id = null;
        obj.course_name = null;
        obj.course_url = null;
        obj.mentor_id = null;
    }

    records.push(obj);
    return records;
}).then((result) => {
    const finalResults = result[0];
    return csvWriter.writeRecords(finalResults).then(() => console.log('Done'))
});